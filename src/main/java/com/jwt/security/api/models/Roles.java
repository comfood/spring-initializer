package com.jwt.security.api.models;

public enum Roles {
    ROLE_ADMIN("ADMIN"),
    ROLE_ADMINISTRATOR("ADMINISTRATOR"),
    ROLE_COLLABORATOR("COLLABORATOR");

    public final String value;

    Roles(String value) {
        this.value = value;
    }
}
